<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-lazyload
// Langue: fr
// Date: 28-04-2017 14:45:52
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'lazyload_description' => 'Ce plugin permet d\'utiliser le script jQuery Lazy Load dans les pages publiques de votre site.
_ http://www.appelsiini.net/projects/lazyload
_ Icône de www.iconshock.com',
	'lazyload_slogan' => 'Ce plugin permet d\'utiliser le script jQuery Lazy Load dans les pages publiques de votre site',
);
?>